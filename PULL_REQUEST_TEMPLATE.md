## Purpose
_Describe the problem or feature in addition to a link to the issues._

## Approach
_How does this change address the problem?_

#### Open Questions and Pre-Merge TODOs
- [ ] Use github checklists. When solved, check the box and explain the answer.
- [ ] Use the following format: `* [owner/repo](link)`
- [ ] Link additions should be added to the bottom of the relevant category.
- [ ] New categories or improvements to the existing categorization are welcome.
- [ ] Search previous suggestions before making a new one, as yours may be a duplicate.
- [ ] Sort by alphabetical order

## Learning
_Describe the research stage_

_Links to blog posts, patterns, libraries or addons used to solve this problem_

#### Blog Posts
- [How to Pull Request](https://github.com/flexyford/pull-request) Github Repo with Learning focused Pull Request Template.

##
Thanks for contributing!
