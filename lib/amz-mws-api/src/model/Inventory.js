const DefaultApi = require('../api/DefaultApi')

// TODO jsdoc
// TODO all methods

/**
 * @summary Fulfillment Inventory API implementation
 * @description The ListInventorySupply operation returns information about the availability of inventory that a seller has in Amazon's fulfillment network and in current inbound shipments. You can check the current availabilty status for your Fulfillment by Amazon inventory as well as discover when availability status changes.
 * @class FulfillmentInventory
 * {@link http://docs.developer.amazonservices.com/en_AU/fba_inventory/FBAInventory_ListInventorySupply.html API reference}
 */
class FulfillmentInventory extends DefaultApi {

	/**
	 * @param {Credentials} credentials Credentials to be used on the API
	 * @param {('BR' | 'CA' | 'MX' | 'US' | 'AE' | 'DE' | 'ES' | 'FR' | 'GB' | 'IT' | 'EG' | 'NL' | 'SA' | 'SE' | 'TR' | 'IN' | 'SG' | 'AU' | 'JP')} marketplaceCountryCode Country code of desired marketplace to be used in API
	 */
	constructor(credentials, marketplaceCountryCode) {
		super(credentials, marketplaceCountryCode, 'FulfillmentInventory')
	}

	async ListInventorySupply(SellerSkus, QueryStartDateTime, ResponseGroup = undefined, MarketplaceId = undefined) {
		const action = this.getActionName()
		const params = {}
		for (let i = 0; i < SellerSkus.length; i += 1) {
			params[`SellerSkus.member.${i + 1}`] = SellerSkus[i]
		}
		if (QueryStartDateTime) params.QueryStartDateTime = QueryStartDateTime
		if (ResponseGroup) params.ResponseGroup = ResponseGroup
		if (MarketplaceId) params.MarketplaceId = MarketplaceId
		const response = await this.request({
			Api: this.api,
			Action: 'ListInventorySupply',
			Params: params,
		})

		return response[`${action}Response`][`${action}Result`].InventorySupplyList.member
	}

}

module.exports = FulfillmentInventory
