const ROOT_PATH = require('app-root-path').path
const fs = require('fs-extra')
const path = require('path')
const jsonfile = require('jsonfile')
const DefaultApi = require('../api/DefaultApi')

class SpReports extends DefaultApi {

	/**
	 * Set Amazon Advertising credentials and api param
	 * @param {Credentials} credentials Amazon Advertising credentials
	 */
	constructor(credentials) {
		super(credentials)
		fs.ensureDirSync(path.join(ROOT_PATH, 'cache'))
		this.api = 'reports'
	}

	async requestReport(profileId, reportType, reportOptions) {
		if (profileId) this.headers['Amazon-Advertising-API-Scope'] = profileId

		const url = this.createUrl([ reportType, 'report' ], 'sp')
		const requestItems = {
			headers: this.headers,
			url,
			responseType: 'json',
			method: 'POST',
			data: reportOptions,
		}
		const response = await this.request(requestItems)

		return response.data
	}

	async getReport(reportId, profileId = undefined) {
		if (profileId) this.headers['Amazon-Advertising-API-Scope'] = profileId
		const url = this.createUrl([ this.api, reportId ], 'sp')
		const requestItems = {
			headers: this.headers,
			url,
			responseType: 'json',
			method: 'GET',
		}
		const response = await this.request(requestItems)

		return response.data
	}

	async getReportTempDownloadURL(locationUrl) {
		const requestItems = {
			headers: this.headers,
			url: locationUrl,
			method: 'GET',
			maxRedirects: 0,
		}
		const response = await this.request(requestItems)

		return response.location
	}

	/**
	 * @param {string} reportType
	 * @param {object} reportOptions
	 * @param {string,int} profileId
	 */
	async processReport(profileId, reportType, reportOptions) {
		const cachedReportFile = path.join(ROOT_PATH, 'cache', `adsReports_${reportType}_${reportOptions.reportDate}.json`)
		if (fs.pathExistsSync(cachedReportFile)) return jsonfile.readFileSync(cachedReportFile)

		this.headers['Amazon-Advertising-API-Scope'] = profileId
		let sleepTimer = 1
		const sleepTimeout = 60
		const requestResponse = await this.requestReport(profileId, reportType, reportOptions)
		let reportResponse = { status: 'IN_PROGRESS' }
		while (reportResponse.status === 'IN_PROGRESS') {
			console.log(reportResponse.status)
			sleepTimer *= 2
			await this.cawer.sleep(sleepTimer)
			if (sleepTimer > sleepTimeout) throw new Error(`SleepTime reached timeout [${sleepTimeout}]`)
			const reportId = requestResponse.reportId
			reportResponse = await this.getReport(reportId)
		}

		if (reportResponse.status === 'SUCCESS') {
			console.log(reportResponse.status)
			const tempDownloadLocation = await this.getReportTempDownloadURL(reportResponse.location)
			const reportData = await this.download(tempDownloadLocation)
			jsonfile.writeFileSync(path.join(ROOT_PATH, 'cache', `adsReports_${reportType}_${reportOptions.reportDate}.json`),
				reportData, { spaces: 2 })
			console.log(`Report ${reportOptions.reportDate} finished`)

			return reportData
		}

		throw new Error(reportResponse.statusDetails)
	}

}

module.exports = SpReports
