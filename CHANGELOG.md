# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 2020-12-16

### Added

- initial version with basic code structure
- sync:
    - products
    - orders
    - products ads campaigns
    - products adGroups
    - products adGroups keywords
    - products adGroups negative keywords
    - products ads campaigns negative keywords
- webserver with a list of your products (port 4000)
- basic readme
- actions on github
- docker image hosted on github and gitlab