const execShPromise = require('exec-sh').promise
const Cawer = require('cawer');

(async () => {
	const cawer = new Cawer()

	await execShPromise('echo date')
	await cawer.sleep(2)
	execShPromise('echo -n Say: && read i && echo Said:$i')
})()
