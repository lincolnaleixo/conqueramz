const path = require('path')
const jsonfile = require('jsonfile')
const SplitTest = require('./splitTest')
const rootPath = path.join(__dirname, '..');

(async () => {
	const configFilePath = path.join(rootPath, 'config.json')
	const config = jsonfile.readFileSync(configFilePath)
	const splitTest = new SplitTest(config)

	await splitTest.splitTestProducts()

	// if (process.argv.find((item) => item === '--orders')) {

	// } else if (process.argv.find((item) => item === '--reports')) {
	// 	process.env.NODE_ENV = 'PRODUCTION'
	// 	console.log('Using default NODE_ENV PRODUCTION')
	// } else if (process.argv.find((item) => item === '--splitTest')) {
	// 	pageType = process.argv[process.argv.findIndex((item) => item === '--pageType') + 1]
	// }
})()
