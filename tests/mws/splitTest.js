/* eslint-disable max-lines-per-function */
const jsonfile = require('jsonfile')
const path = require('path')
// const Configurati = require('configurati')
const moment = require('moment-timezone')
const appRoot = require('app-root-path')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const Core = require('../../src/mws/core')
const splitTestDBPath = path.join(appRoot.path, 'database', 'splitTest.json')
const adapter = new FileSync(splitTestDBPath)
const db = low(adapter)
const Products = require('../../src/mws/inventory.js')

class SplitTest extends Core {

	constructor(config) {
		super(config, 'splitTest')
		this.products = new Products(config)
	}

	async changeSalePrice(options) {
		try {
			const splitTestInfo = {}
			const dateToday = moment(new Date()).tz('America/Los_Angeles')
				.format('YYYY-MM-DD')
			const dateYesterday = moment(new Date()).tz('America/Los_Angeles')
				.subtract(1, 'days')
				.format('YYYY-MM-DD')
			const params = {
				FeedType: '_POST_PRODUCT_PRICING_DATA_',
				SellerId: options.SELLER_ID,
				SKU: options.SKU,
				StandardPrice: options.STANDARD_PRICE,
				SalePrice: options.SALES_PRICE,
				SaleStartDate: options.SALE_START_DATE,
				SaleEndDate: options.SALE_END_DATE,
			}
			const response = await this.mws.request({
				Api: 'Feeds',
				Action: 'SubmitFeed',
				Params: params,
			})
			const cacheFolder = path.join(appRoot.path, 'cache')
			jsonfile.writeFileSync(path.join(cacheFolder, `feeds.json`), response, { spaces: 2 })
			if (response
				.SubmitFeedResponse.SubmitFeedResult.FeedSubmissionInfo.FeedProcessingStatus === '_SUBMITTED_') {
				this.logger.info('Price change requested successfully')
				this.logger.warn('Saving info on database')

				const productOptions = {
					MARKETPLACE_ID: options.MARKETPLACE_ID,
					IdType: 'SellerSKU',
					SKU: options.SKU,
				}
				const productInfo = await this.products.getProductInfo(productOptions)
				const productPricesOptions = {
					MARKETPLACE_ID: options.MARKETPLACE_ID,
					SKU: options.SKU,
				}
				const productPricesInfo = await this.products.getProductPrices(productPricesOptions)

				splitTestInfo.sku = options.SKU
				splitTestInfo.bsr = productInfo.SalesRankings.SalesRank[0].Rank
				splitTestInfo.salesPrice = parseFloat(productPricesInfo.LandedPrice.Amount)
				db.set(dateYesterday, splitTestInfo)
					.write()
				db.set(dateToday, {
					sku: options.SKU,
					bsr: 'still not finished',
					salesPrice: options.SALES_PRICE,
				})
					.write()
			}
		} catch (err) {
			this.logger.error(`Error on changeSalePrice: ${err}`)
		}
	}

	async splitTestProducts() {
		const splitTestList = this.config.SPLIT_TEST
		for (let i = 0; i < splitTestList.length; i += 1) {
			const splitTestItem = splitTestList[i]
			await this.changeSalePrice(splitTestItem)
		}
	}
}

module.exports = SplitTest

// (async () => {
// 	// const configuratiOptionsFile = path.join(__dirname, '..', 'resources', 'configurati.json')
// 	// const configuratiOptions = jsonfile.readFileSync(configuratiOptionsFile)
// 	// configuratiOptions.clientSecretPath = path.join(__dirname, '..', configuratiOptions.clientSecretPath)
// 	// configuratiOptions.gdriveTokenPath = path.join(__dirname, '..', configuratiOptions.gdriveTokenPath)
// 	// const configurati = new Configurati('gsheets', configuratiOptions)
// 	// const config = await configurati.get()
// 	const config = jsonfile.readFileSync(path.join(rootPath, 'config.json'))
// 	logger = new Logering('splitTest', config.LOGGERING)
// 	logger.info('Starting splitTest jobs')
// 	config.LOGGERING.RELEASE_VERSION = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'package.json')).toString()).version
// 	process.env.NODE_ENV = 'PRODUCTION'
// 	const mwsCredentials = {
// 		MWS_AWS_ACCESS_KEY_ID: config.CREDENTIALS.ACCESS_KEY_ID,
// 		MWS_SECRET_ACCESS_KEY: config.CREDENTIALS.SECRET_ACCESS_KEY,
// 		MWS_SELLER_ID: config.CREDENTIALS.SELLER_ID,
// 		MarketplaceId: config.CREDENTIALS.MARKETPLACE_ID,
// 	}
// 	mws = new Mws(mwsCredentials)

// 	await changeSalePrice(mwsCredentials, config.SPLIT_TEST)

// 	// Promise.all([ orders.getAndSaveOrders(mwsCredentials), reports.getAndSaveProductsReports(mwsCredentials) ])
// })()
