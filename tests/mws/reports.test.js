const path = require('path')
const dotenv = require('dotenv')
const moment = require('moment-timezone')
const ROOT_PATH = require('app-root-path').path
const Cawer = require('cawer')
const fs = require('fs-extra')
const DbHelper = require('../../src/helpers/db')
const Products = require('../../src/mws/inventory')
const Reports = require('../../src/mws/reports')
const dbHelper = new DbHelper()

let config = ''
const cawer = new Cawer()
dotenv.config();

(async () => {
	await dbHelper.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
	})
	config = await dbHelper.getConfig()
	await fs.ensureDirSync(path.join(ROOT_PATH, 'logs'))
	const reports = new Reports(config)
	const reportType = '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_'
	const startDate = moment().tz('America/Los_Angeles')
		.subtract('20', 'days')
		.format('YYYY-MM-DDT00:00:00.000')
	console.log(await reports.processReport(reportType, startDate))

	// const dateSync = moment().tz('America/Los_Angeles')
	// 	.subtract('20', 'days')
	// 	.format('YYYY-MM-DDT23:59:59.999')
	// await reports.processAndSaveAllInventoryReports(dbHelper)
	// await dbHelper.insertDocument('products', dateSync, items)

	process.exit(0)

	// if (process.argv.find((item) => item === '--orders')) {

	// } else if (process.argv.find((item) => item === '--reports')) {
	// 	process.env.NODE_ENV = 'PRODUCTION'
	// 	console.log('Using default NODE_ENV PRODUCTION')
	// } else if (process.argv.find((item) => item === '--splitTest')) {
	// 	pageType = process.argv[process.argv.findIndex((item) => item === '--pageType') + 1]
	// }
})()
