const path = require('path')
const dotenv = require('dotenv')
const moment = require('moment-timezone')
const ROOT_PATH = require('app-root-path').path
const Cawer = require('cawer')
const fs = require('fs-extra')
const DbHelper = require('../../src/helpers/db')
const Orders = require('../../src/mws/orders')
const dbHelper = new DbHelper()

let config = ''
const cawer = new Cawer()
dotenv.config()

async function getOrders() {
	await dbHelper.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
	})
	config = await dbHelper.getConfig()
	await fs.ensureDirSync(path.join(ROOT_PATH, 'logs'))

	const orders = new Orders(config)
	const items = await orders.getOrders('past hour')
	const dateOrders = moment().tz('America/Los_Angeles')
		.subtract(1, 'days')
		.format('YYYYMMDD-HH')

	await dbHelper.insertDocument('orders', dateOrders, items)

	console.log('ended')
}

(async () => {
	await getOrders()
	process.exit(0)
})()
