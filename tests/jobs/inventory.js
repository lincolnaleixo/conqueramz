const path = require('path')
const dotenv = require('dotenv')
const ROOT_PATH = require('app-root-path').path
const fs = require('fs-extra')
const inventoryJobs = require('../../src/jobs/inventory')
const DB = require('../../src/helpers/db')
const db = new DB()

let config = ''
dotenv.config();

(async () => {
	await db.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
		MONGODB_PASSWORD: process.env.MONGODB_PASSWORD,
		MONGODB_USERNAME: process.env.MONGODB_USERNAME,
	})
	config = await db.getConfig()
	await fs.ensureDirSync(path.join(ROOT_PATH, 'logs'))

	await inventoryJobs.daily(config, db)

	process.exit(0)
})()

