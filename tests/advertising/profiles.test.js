const jsonfile = require('jsonfile')
const path = require('path')
const rootPath = path.join(__dirname, '..', '..')
const Cawer = require('cawer')
const dotenv = require('dotenv')
const AdsProfiles = require('../../src/advertising/profiles')
const DbHelper = require('../../src/helpers/db')
const dbHelper = new DbHelper()
let config = ''
const cawer = new Cawer()
dotenv.config();

(async () => {
	await dbHelper.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
	})
	config = await dbHelper.getConfig()

	// const configFilePath = path.join(rootPath, 'config.json')
	// const config = jsonfile.readFileSync(configFilePath)
	const adsProfiles = new AdsProfiles(config)

	console.log(await adsProfiles.getAll())
})()
