const path = require('path')
const dotenv = require('dotenv')
const moment = require('moment-timezone')
const ROOT_PATH = require('app-root-path').path
const Cawer = require('cawer')
const fs = require('fs-extra')
const DbHelper = require('../../../src/helpers/db')
const AdGroupsNegativeKeywords = require('../../../src/advertising/products/adGroupNegativeKeywords')
const dbHelper = new DbHelper()

let config = ''
const cawer = new Cawer()
dotenv.config();

(async () => {
	await dbHelper.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
	})
	config = await dbHelper.getConfig()
	await fs.ensureDirSync(path.join(ROOT_PATH, 'logs'))

	const dateSync = moment().tz('America/Los_Angeles')
		.format('YYYYMMDD')
	const adGroupNegativeKeywords = new AdGroupsNegativeKeywords(config)
	const items = await adGroupNegativeKeywords.getAll()
	await dbHelper.insertDocument('adGroupsNegativeKeywords', dateSync, items)

	process.exit(0)

	// if (process.argv.find((item) => item === '--orders')) {

	// } else if (process.argv.find((item) => item === '--reports')) {
	// 	process.env.NODE_ENV = 'PRODUCTION'
	// 	console.log('Using default NODE_ENV PRODUCTION')
	// } else if (process.argv.find((item) => item === '--splitTest')) {
	// 	pageType = process.argv[process.argv.findIndex((item) => item === '--pageType') + 1]
	// }
})()
