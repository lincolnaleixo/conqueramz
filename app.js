const express = require('express')
const exphbs = require('express-handlebars')
const Logering = require('logering')
const bodyParser = require('body-parser')
const app = express()
const dotenv = require('dotenv')
const jsonfile = require('jsonfile')
const port = process.env.PORT || 4000
const fs = require('fs')
const path = require('path')
const appRoot = require('app-root-path')
const DbHelper = require('./src/helpers/db')
const db = new DbHelper()
let config = ''
dotenv.config()

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*')
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	next()
})

app.engine('hbs', exphbs({
	defaultLayout: 'main',
	extname: '.hbs',
}))

app.set('view engine', 'hbs')

// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

app.use(express.static(path.join(__dirname, '/public')))

app.get('/', async (req, res) => {
	const inventoryList = (await db.getLastInsertedDocument('inventory'))[0].items
	res.render('inventory', { products: inventoryList })
})

// // request handlers
// app.get('/', (req, res) => {
// 	res
// 		.status(200)
// 		.json({ message: 'ConquerAmazon' })
// })

app.listen(port, () => {
	console.log(`Server started on: ${port}`)
})

process.on('uncaughtException', (e) => {
	console.log('An error has occurred. error is: %s and stack trace is: %s', e, e.stack)
	console.log('Process will restart now.')
	process.exit(1)
})

async function main() {
	await db.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
		MONGODB_PASSWORD: process.env.MONGODB_PASSWORD,
		MONGODB_USERNAME: process.env.MONGODB_USERNAME,
	})
	config = await db.getConfig()

	this.logger = new Logering('webserver', config.LOGERING)
}

(async () => {
	if (require.main === module) {
		await main()
	}
})()
