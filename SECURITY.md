# Security Policy

## NodeJS Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| >= 14   | :x:                |
| 12.x.x  | :white_check_mark: |
| < 12    | :x:                |
