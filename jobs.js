const { CronJob } = require('cron')
const AlertGuy = require('alertguy')
const moment = require('moment-timezone')
const path = require('path')
const dotenv = require('dotenv')
const Logering = require('logering')
const ROOT_PATH = require('app-root-path').path
const Cawer = require('cawer')
const fs = require('fs-extra')
const DbHelper = require('./src/helpers/db')
const dbHelper = new DbHelper()
const inventoryJobs = require('./src/jobs/inventory')
let config = ''
const cawer = new Cawer()
dotenv.config()
const cronJobsList = []

function isAnyJobRunning() {
	for (const cronjobName in cronJobsList) {
		if (cronJobsList[cronjobName].isRunning) return true
	}

	return false
}

function startInventoryJobs(cronjobsSchedules) {
	cronJobsList.INVENTORY_DAILY = new CronJob({
		cronTime: cronjobsSchedules.INVENTORY_DAILY,
		onTick: async () => {
			if (cronJobsList.INVENTORY_DAILY.taskRunning) return

			cronJobsList.INVENTORY_DAILY.taskRunning = true

			try {
				this.logger.info('Starting inventory sync task (INVENTORY_DAILY)')
				await inventoryJobs.daily(config, dbHelper)
				this.logger.info('Inventory sync task (INVENTORY_DAILY) ended')
			} catch (e) {
				this.logger.error(e)
			}

			cronJobsList.INVENTORY_DAILY.taskRunning = false
		},
		start: true,
		timeZone: 'America/Los_Angeles',
	})
}

function systemJobs(cronjobsSchedules) {
	cronJobsList.RESTART = new CronJob({
		cronTime: cronjobsSchedules.RESTART,
		onTick: () => {
			if (cronJobsList.RESTART.taskRunning) return

			cronJobsList.RESTART.taskRunning = true

			try {
				this.logger.info('Starting restart task')

				if (!isAnyJobRunning()) {
					console.log('No job running')
					console.log('Restarting...')
					process.exit(0)
				}
			} catch (e) {
				console.log('Error on executing jobs task restart, forcing restart')
				process.exit(0)
			}

			cronJobsList.RESTART.taskRunning = false

			this.logger.info('Restart task ended')
		},
		start: true,
		timeZone: 'America/Los_Angeles',
	})
}

async function startCronjobs(cronjobsSchedules) {
	this.logger.info('Cronjob started. Waiting for the next execution')
	this.logger.info(JSON.stringify(cronjobsSchedules))

	systemJobs(cronjobsSchedules)
	await startInventoryJobs(cronjobsSchedules)
}

async function main() {
	await dbHelper.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
		MONGODB_PASSWORD: process.env.MONGODB_PASSWORD,
		MONGODB_USERNAME: process.env.MONGODB_USERNAME,
	})
	config = await dbHelper.getConfig()
	const cronjobsSchedules = config.JOBS
	await fs.ensureDirSync(path.join(ROOT_PATH, 'logs'))
	this.logger = new Logering('app', config.LOGERING)

	await startCronjobs(cronjobsSchedules)
}

(async () => {
	if (require.main === module) {
		await main()
	}
})()
