const moment = require('moment-timezone')
const Logering = require('logering')
const Cawer = require('cawer')
const MWS = require('../../lib/amz-mws-api/src')
const Reports = require('./reports')
const inventoryReports = [
	'_GET_FLAT_FILE_OPEN_LISTINGS_DATA_',
	'_GET_MERCHANT_LISTINGS_ALL_DATA_',
	'_GET_MERCHANT_LISTINGS_DATA_',
	'_GET_MERCHANT_LISTINGS_INACTIVE_DATA_',
]

class Products {

	constructor(config) {
		this.logger = new Logering('Inventory', config.LOGERING)
		this.MWS = new MWS(config.CREDENTIALS, 'US')
		this.config = config
		this.cawer = new Cawer()
		this.reports = new Reports(config)
	}

	async getListInventoryInfo() {
		try {
			const offsetUTCLA = 0 + ((moment.tz('America/Los_Angeles').utcOffset() / 60) * -1).toString()
			const date = moment()
				.subtract(1, 'days')
				.tz('America/Los_Angeles')
			const response = await this.MWS.Inventory
				.ListInventorySupply([], date.format(`YYYY-MM-DDT23:59:59.999-${offsetUTCLA}`))

			return response
		} catch (err) {
			throw new Error(`Error on getListInventoryInfo: ${err}`)
		}
	}

}

module.exports = Products
