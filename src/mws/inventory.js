const path = require('path')
const fs = require('fs-extra')
const moment = require('moment-timezone')
const ROOT_PATH = require('app-root-path').path
const Logering = require('logering')
const Cawer = require('cawer')
const MWS = require('../../lib/amz-mws-api/src')
const Reports = require('./reports')

class Inventory {

	constructor(config) {
		this.logger = new Logering('Inventory', config.LOGERING)
		this.MWS = new MWS(config.CREDENTIALS, 'US')
		this.config = config
		this.cawer = new Cawer()
		this.reports = new Reports(config)
	}

	async getProductInfo(options) {
		try {
			const api = 'Products'
			const action = 'GetMatchingProductForId'
			const response = await this.mws.request({
				Api: api,
				Action: action,
				Params: {
					MarketplaceId: options.MARKETPLACE_ID,
					IdType: options.IdType,
					'IdList.Id.1': options.SKU,
				},
			})

			return response
				.GetMatchingProductForIdResponse
				.GetMatchingProductForIdResult
				.Products
				.Product
		} catch (err) {
			throw new Error(`Error on getProductInfo: ${err}`)
		}
	}

	async getProductPrices(options) {
		try {
			const api = 'Products'
			const action = 'GetMyPriceForSKU'
			const response = await this.mws.request({
				Api: api,
				Action: action,
				Params: {
					MarketplaceId: options.MARKETPLACE_ID,
					'SellerSKUList.SellerSKU.1': options.SKU,
				},
			})

			return response
				.GetMyPriceForSKUResponse
				.GetMyPriceForSKUResult
				.Product
				.Offers
				.Offer
				.BuyingPrice
		} catch (err) {
			throw new Error(`Error on getProductPrices: ${err}`)
		}
	}

	async generatingInventoryReports() {
		this.logger.info('Getting products reports info')
		let reportType = '_GET_MERCHANT_LISTINGS_ALL_DATA_'
		const reportInfo = await this.reports.processReport(reportType, undefined, undefined, 300)
		// this.logger.info(`Report ${reportType} info saved successfully`)
		reportType = '_GET_MERCHANT_LISTINGS_DEFECT_DATA_'
		const reportInfo2 = await this.reports.processReport(reportType, undefined, undefined, 300)
		// this.logger.info(`Report ${reportType} info saved successfully`)
		reportType = '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_'
		const reportInfo3 = await this.reports.processReport(reportType, undefined, undefined, 300)
		// this.logger.info(`Report ${reportType} info saved successfully`)
		reportType = '_GET_AFN_INVENTORY_DATA_'
		const reportInfo4 = await this.reports.processReport(reportType, undefined, undefined, 300)

		// this.logger.warn(`Report ${reportType} info saved successfully`)
		return {
			reportInfo, reportInfo2, reportInfo3, reportInfo4,
		}
	}

	async changeSalePrice(options) {
		try {
			const splitTestInfo = {}
			const dateToday = moment(new Date()).tz('America/Los_Angeles')
				.format('YYYY-MM-DD')
			const dateYesterday = moment(new Date()).tz('America/Los_Angeles')
				.subtract(1, 'days')
				.format('YYYY-MM-DD')
			const params = {
				FeedType: '_POST_PRODUCT_PRICING_DATA_',
				SellerId: options.SELLER_ID,
				SKU: options.SKU,
				StandardPrice: options.STANDARD_PRICE,
				SalePrice: options.SALES_PRICE,
				SaleStartDate: options.SALE_START_DATE,
				SaleEndDate: options.SALE_END_DATE,
			}
			const response = await this.mws.request({
				Api: 'Feeds',
				Action: 'SubmitFeed',
				Params: params,
			})
			const cacheFolder = path.join(appRoot.path, 'cache')
			jsonfile.writeFileSync(path.join(cacheFolder, `feeds.json`), response, { spaces: 2 })
			if (response
				.SubmitFeedResponse.SubmitFeedResult.FeedSubmissionInfo.FeedProcessingStatus === '_SUBMITTED_') {
				this.logger.info('Price change requested successfully')
				this.logger.warn('Saving info on database')

				const productOptions = {
					MARKETPLACE_ID: options.MARKETPLACE_ID,
					IdType: 'SellerSKU',
					SKU: options.SKU,
				}
				const productInfo = await this.products.getProductInfo(productOptions)
				const productPricesOptions = {
					MARKETPLACE_ID: options.MARKETPLACE_ID,
					SKU: options.SKU,
				}
				const productPricesInfo = await this.products.getProductPrices(productPricesOptions)

				splitTestInfo.sku = options.SKU
				splitTestInfo.bsr = productInfo.SalesRankings.SalesRank[0].Rank
				splitTestInfo.salesPrice = parseFloat(productPricesInfo.LandedPrice.Amount)
				db.set(dateYesterday, splitTestInfo)
					.write()
				db.set(dateToday, {
					sku: options.SKU,
					bsr: 'still not finished',
					salesPrice: options.SALES_PRICE,
				})
					.write()
			}
		} catch (err) {
			this.logger.error(`Error on changeSalePrice: ${err}`)
		}
	}

	// TODO [#17]: refactor para usar for in e ficar mais claro
	async getInventoryListInfo() {
		const {
			reportInfo, reportInfo2, reportInfo3, reportInfo4,
		} = await this.generatingInventoryReports()

		this.logger.info('Merging reports infos')
		for (let i = 0; i < reportInfo.length; i += 1) {
			const productInfo = reportInfo[i]
			for (let j = 0; j < reportInfo2.length; j += 1) {
				const productInfo2 = reportInfo2[j]
				if (productInfo['seller-sku'] === productInfo2.sku) {
					reportInfo[i] = {
						...reportInfo[i], ...productInfo2,
					}
				}
			}
			for (let k = 0; k < reportInfo3.length; k += 1) {
				const productInfo3 = reportInfo3[k]
				if (productInfo['seller-sku'] === productInfo3.sku) {
					reportInfo[i] = {
						...reportInfo[i], ...productInfo3,
					}
				}
			}
			for (let j = 0; j < reportInfo4.length; j += 1) {
				const productInfo4 = reportInfo4[j]
				if (productInfo['seller-sku'] === productInfo4['seller-sku']) {
					reportInfo[i] = {
						...reportInfo[i], ...productInfo4,
					}
				}
			}
		}
		this.logger.info('Done merging reports infos')

		return reportInfo
	}

	async getAllSellerAsins(db) {
		// First check database
		let sellerAsins = await db.getLastInsertedDocument('products')
		if (sellerAsins.length > 0) return [ ...new Set(sellerAsins[0].items.map((item) => item.asin1)) ]
		// TODO test when no db
		sellerAsins = await this.getInventoryListInfo()

		return sellerAsins
	}

	// async getListingQualityAndSuppressedListing() {
	// 	const reportType = '_GET_MERCHANT_LISTINGS_DEFECT_DATA_'
	// 	// First check database
	// 	const reportInfo = await this.reports.processReport(this.config.CREDENTIALS, reportType)
	//
	// 	return reportInfo
	// }
}

module.exports = Inventory
