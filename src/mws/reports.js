const Cawer = require('cawer')
const jsonfile = require('jsonfile')
const path = require('path')
const fs = require('fs-extra')
const moment = require('moment-timezone')
const ROOT_PATH = require('app-root-path').path
const Logering = require('logering')
const MWS = require('../../lib/amz-mws-api/src')
const cacheFolder = path.join(ROOT_PATH, 'cache', 'reports')
const dateNow = moment(new Date())
	.tz('America/Los_Angeles')
	.format('YYYYMMDD')

class Reports {

	constructor(config) {
		this.logger = new Logering('Reports', config.LOGERING)
		this.MWS = new MWS(config.CREDENTIALS, 'US')
		this.config = config
		this.cawer = new Cawer()
	}

	getReportsTypes(category) {
		return jsonfile.readFileSync(path.join(ROOT_PATH, 'resources', 'reports.json'))[category.toUpperCase()]
	}

	async processAndSaveAllInventoryReports(db) {
		let reportInfo
		const dateSync = moment().tz('America/Los_Angeles')
			.format('YYYYMMDD')
		const reportsTypes = this.getReportsTypes('INVENTORY')
		for (let i = 0; i < reportsTypes.length; i += 1) {
			const reportType = reportsTypes[i]

			reportInfo = await this.processReport(this.config.CREDENTIALS, reportType, 300)
			await db.insertDocument(reportType, dateSync, reportInfo)
		}
	}

	async requestReport(reportType, startDate = undefined, endDate = undefined) {
		let reportRequestId
		this.logger.info(`Requesting report type ${reportType}`)

		const jsonResponse = await this.MWS.Reports
			.requestReport(reportType, startDate, endDate)

		try {
			reportRequestId = jsonResponse.ReportRequestInfo.ReportRequestId
		} catch (err) {
			this.logger.error(`Error on ${err}`)
		}

		return reportRequestId
	}

	async getLastAvailableReport(reportType) {
		try {
			const jsonResponse = await this.MWS.Reports
				.requestReport('GetReportList', undefined, undefined, { 'ReportTypeList.Type.1': reportType })
			const reportId = jsonResponse.GetReportListResponse.GetReportListResult.ReportInfo[0].ReportId

			return reportId
		} catch (err) {
			throw new Error(`Error on getLastAvailableReport: ${err}`)
		}
	}

	async waitReportCompletion(reportRequestId, reportType) {
		this.logger.info(`Waiting for completion of report request id: ${reportRequestId}`)
		// const date = new Date()
		let sleeptimer = 2
		while (true) {
			const jsonResponse = await this.MWS.Reports.getReportRequestList([ reportRequestId ], [ reportType ])
			const reportProcessingStatus = jsonResponse.ReportRequestInfo.ReportProcessingStatus
			this.logger.info(`${reportRequestId} status: ${reportProcessingStatus}`)
			if (reportProcessingStatus === '_DONE_') {
				return jsonResponse.ReportRequestInfo.GeneratedReportId
			}
			if (reportProcessingStatus === '_CANCELLED_') {
				this.logger.warn('Report request cancelled. Trying to get last report available')
				const reportId = await this.getLastAvailableReport(reportType)

				return reportId
			}
			if (reportProcessingStatus === '_IN_PROGRESS_' || reportProcessingStatus === '_SUBMITTED_') {
				this.logger.warn(`Sleeping for ${sleeptimer} seconds and checking again...`)
				this.cawer.sleep(sleeptimer)
				sleeptimer *= 2
			} else {
				return { Status: reportProcessingStatus }
			}
		}
	}

	async getReport(generatedReportId) {
		this.logger.info('Done. Getting report information')

		return this.MWS.Reports.getReport(generatedReportId)

		// const jsonResponse = await this.mws.request({
		// 	Api: 'Reports',
		// 	Action: 'GetReport',
		// 	Params: { ReportId: generatedReportId },
		// })

		// return jsonResponse
	}

	getMinutesFileSaved(fullPath) {
		const fileStats = fs.statSync(fullPath)
		const dateFileCreation = moment.utc(fileStats.atime).format('YYYY-MM-DDTHH:mm:ss')
		const dateNowCache = moment().utc()
			.format('YYYY-MM-DDTHH:mm:ss')
		const duration = moment.duration(moment(dateNowCache).diff(moment(dateFileCreation)))

		return duration.asMinutes()
	}

	async processReport(reportType, startDate = undefined, endDate = undefined, cacheMinutes = 30) {
		this.logger.info(`Processing ${reportType} info`)
		// if (reportCategory !== undefined) cacheFolder = path.join(cacheFolder, reportCategory)
		let reportInfo
		const cacheFileFullPath = path.join(cacheFolder, `${reportType}.json`)
		if (cacheMinutes && fs.existsSync(cacheFileFullPath)) {
			const minutes = this.getMinutesFileSaved(cacheFileFullPath)
			if (minutes <= cacheMinutes) {
				this.logger.info(`Report ${reportType} cache is valid`)

				return jsonfile.readFileSync(cacheFileFullPath)
			}
		}
		const reportRequestId = await this.requestReport(reportType, startDate, endDate)
		const reportResponse = await this.waitReportCompletion(reportRequestId, reportType)
		if (typeof reportResponse === 'string') {
			const generatedReportId = reportResponse
			reportInfo = await this.getReport(generatedReportId)
		} else reportInfo = reportResponse

		if (cacheMinutes) {
			fs.ensureDirSync(cacheFolder)
			jsonfile.writeFileSync(cacheFileFullPath, reportInfo, { spaces: 2 })
		}

		return reportInfo
	}

	saveInfoDatabase(reportInfo) {
		this.logger.info('Saving report information on database')
		const databaseFolder = path.join(ROOT_PATH, 'database', 'core')
		const databaseFile = `products-${dateNow}.json`
		const databaseFullPath = path.join(databaseFolder, databaseFile)
		// const adapter = new FileSync(databaseFullPath)
		// const db = low(adapter)

		// db.setState(reportInfo).write()

		this.logger.warn(`Reports information saved`)
	}

	// async getAndSaveProductsReports() {
	// 	const reportInfo = await this.getProductsReports()
	// 	await this.saveInfoDatabase(reportInfo)
	// }

}

module.exports = Reports
