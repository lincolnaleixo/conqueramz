const jsonfile = require('jsonfile')
const path = require('path')
const fs = require('fs-extra')
const moment = require('moment-timezone')
const ROOT_PATH = require('app-root-path').path
const Logering = require('logering')
const Cawer = require('cawer')
const MWS = require('../../lib/amz-mws-api/src')
const cacheFolder = path.join(ROOT_PATH, 'cache', 'orders')

class Orders {

	constructor(config) {
		this.logger = new Logering('Orders', config.LOGERING)
		this.MWS = new MWS(config.CREDENTIALS, 'US')
		this.config = config
		this.cawer = new Cawer()
	}

	formatDates(range = 'yesterday') {
		const offsetUTCLA = 0 + ((moment.tz('America/Los_Angeles').utcOffset() / 60) * -1).toString()

		if (range.indexOf('last') > -1 && range.indexOf('days') > -1) {
			const days = parseInt(range.split(' ')[1], 10)
			const afterDate = moment()
				.tz('America/Los_Angeles')
				.subtract(days, 'days')
			const beforeDate = moment()
				.tz('America/Los_Angeles')
				.subtract(1, 'days')

			return {
				beforeDate: beforeDate.format(`YYYY-MM-DDT23:59:59.999-${offsetUTCLA}`),
				afterDate: afterDate.format(`YYYY-MM-DDT00:00:00.000-${offsetUTCLA}`),
			}
		}
		if (range === 'yesterday') {
			const yesterDayDate = moment()
				.tz('America/Los_Angeles')
				.subtract(1, 'days')

			return {
				beforeDate: yesterDayDate.format('YYYY-MM-DDT23:59:59.999'),
				afterDate: yesterDayDate.format('YYYY-MM-DDT00:00:00.000'),
			}
		}
		if (range === 'past hour') {
			const beforeDate = moment()
				.tz('America/Los_Angeles')
				.subtract(1, 'hours')
				.format('YYYY-MM-DDTHH:59:59.999')
			const afterDate = moment()
				.tz('America/Los_Angeles')
				.subtract(1, 'hours')
				.format('YYYY-MM-DDTHH:00:00.00')

			return {
				beforeDate,
				afterDate,
			}
		}

		throw new Error('No valid range specified')
	}

	getMinutesFileSaved(fullPath) {
		const fileStats = fs.statSync(fullPath)
		const dateFileCreation = moment.utc(fileStats.atime).format('YYYY-MM-DDTHH:mm:ss')
		const dateNowCache = moment().utc()
			.format('YYYY-MM-DDTHH:mm:ss')
		const duration = moment.duration(moment(dateNowCache).diff(moment(dateFileCreation)))

		return duration.asMinutes()
	}

	checkIfHasCache(range, cacheMinutes) {
		const cacheFileFullPath = path.join(cacheFolder, `${range}.json`)
		if (cacheMinutes && fs.existsSync(cacheFileFullPath)) {
			const minutes = this.getMinutesFileSaved(cacheFileFullPath)
			if (minutes <= cacheMinutes) {
				this.logger.info(`Orders ${range} cache is valid`)

				return jsonfile.readFileSync(cacheFileFullPath)
			}
		}

		return false
	}

	// *possibles options.range values*
	// yesterday: yesterday orders
	// last X days: last X orders. X days past until yesterday
	async getOrders(range, cacheMinutes = 15) {
		this.logger.info(`Getting orders from ${range} range`)
		const dates = this.formatDates(range)

		try {
			if (range === 'yesterday') {
				const cache = this.checkIfHasCache(range, cacheMinutes)
				if (cache !== false) return cache
			}

			let ordersList = await this.MWS.Orders.listOrders(dates.afterDate, dates.beforeDate)
			if (ordersList.Orders.Order && ordersList.Orders.Order.AmazonOrderId) {
				ordersList = [ ordersList.Orders.Order ]
			} else if (ordersList.Orders.Order) ordersList = ordersList.Orders.Order
			else ordersList = []

			for (let i = 0; i < ordersList.length; i += 1) {
				const order = ordersList[i]
				this.logger.info(`Getting orders items from order ${order.AmazonOrderId}`)
				const orderItemsList = await this.MWS.Orders.listOrderItems(order.AmazonOrderId)
				ordersList[i] = {
					...ordersList[i], ...{ Items: orderItemsList },
				}
				await this.cawer.sleep(2)
			}

			return ordersList
		} catch (err) {
			this.logger.error(`Error on ${err}`)
			throw new Error(err)
		}
	}

}

module.exports = Orders

