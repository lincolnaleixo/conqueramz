const MongoClient = require('mongodb').MongoClient
const ObjectID = require('mongodb').ObjectID

class Db {

	constructor() {
		this.db = ''
	}

	async connectToServer(serverInfo) {
		// const url = `mongodb://${serverInfo.MONGODB_SERVER}:${serverInfo.MONGODB_PORT}`
		// const url = `mongodb+srv://${serverInfo.MONGODB_USERNAME}:${serverInfo.MONGODB_PASSWORD}@${serverInfo.MONGODB_SERVER}/${serverInfo.MONGODB_DBNAME}?retryWrites=true&w=majority`
		const url = `mongodb://${serverInfo.MONGODB_USERNAME}:${serverInfo.MONGODB_PASSWORD}@${serverInfo.MONGODB_SERVER}/${serverInfo.MONGODB_DBNAME}?authSource=admin`
		// const url `mongodb://[username:password@]host1[:port1][,...hostN[:portN]][/[defaultauthdb][?options]]`
		// const url = `mongodb+srv://${serverInfo.MONGODB_USERNAME}:${serverInfo.MONGODB_PASSWORD}@${serverInfo.MONGODB_SERVER}/${serverInfo.MONGODB_DBNAME}?retryWrites=true&w=majority`
		const dbName = serverInfo.MONGODB_DBNAME
		const client = new MongoClient(url, { useUnifiedTopology: true })
		await client.connect()
		this.db = client.db(dbName)

		return this.db
	}

	getConfig() {
		return this.db.collection('config').findOne({})
	}

	async insertNewConfigItem(object) {
		const collectionName = 'config'
		const id = (await this.db.collection('config').findOne({}))._id

		await this.db.collection('config').updateOne({ _id: new ObjectID(id) },
			{ $set: object }, {
				upsert: true, multi: true,
			})

		this.db.collection(collectionName)
	}

	async getDocFromACollectionById(collectionName, id) {
		const docRaw = await this.db.collection(collectionName).find({ _id: id })
		const docArray = await docRaw.toArray()
		const object = docArray[0]

		return object
	}

	async getLastInsertedDocument(collectionName) {
		const doc = await this.db.collection(collectionName).find({})
			.sort({ _id: -1 })
			.limit(1)

		return doc.toArray()
	}

	insertDocument(collectionName, id, items) {
		return this.db.collection(collectionName).replaceOne({ _id: id }, { items }, { upsert: true })
	}

	async createSystemCollections() {
		const systemCollections = [ 'products' ]
		for (let i = 0; i < systemCollections.length; i += 1) {
			await this.createCollection(systemCollections[i])
		}

		// await db.createCollection('products')
	}

	async createCollection(collectionName) {
		if (!await this.isCollectionCreated(collectionName)) await this.db.createCollection(collectionName)
	}

	async isCollectionCreated(collectionName) {
		const exists = (await (await this.db
			.listCollections()
			.toArray())
			.findIndex((item) => item.name === collectionName) !== -1)

		return exists
	}

}

module.exports = Db
