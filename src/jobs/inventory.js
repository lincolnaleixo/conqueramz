const moment = require('moment-timezone')
const Inventory = require('../mws/inventory')

module.exports = { async daily(config, db) {
	try {
		const dateSync = moment().tz('America/Los_Angeles')
			.format('YYYYMMDD')
		const inventory = new Inventory(config)
		const items = await inventory.getInventoryListInfo()
		await db.insertDocument('inventory', dateSync, items)
	} catch (e) {
		throw new Error(`Error on executing jobs task products: ${e}`)
	}
} }
