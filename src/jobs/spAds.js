const { CronJob } = require('cron')
const AlertGuy = require('alertguy')
const moment = require('moment-timezone')
const path = require('path')
const dotenv = require('dotenv')
const ROOT_PATH = require('app-root-path').path
const Cawer = require('cawer')
const fs = require('fs-extra')
const Products = require('../mws/inventory')
const AdsSpAsins = require('../advertising/products/asins')
const AdsSpReports = require('../advertising/products/reports')
// const Orders = require('../src/mws/orders')
// const Inventory = require('../src/mws/inventory')
// const AdsAsin = require('../src/advertising/products/S')
// const AdsSpCampaigns = require('../src/advertising/products/campaigns')
// const AdsSpGroups = require('../src/advertising/products/adGroups')
// const AdsSpGroupsKeywords = require('../src/advertising/products/adGroupKeywords')
// const AdsSpGroupsNegativeKeywords = require('../src/advertising/products/adGroupNegativeKeywords')
// const AdsSpCampaignsNegativeKeywords = require('../src/advertising/products/campaignsNegativeKeywords')
// const AdsReports = require('../src/advertising/products/reports')
// const AdsReports = require('../src/advertising/products/reports')
// const AdsAutomation = require('../src/automation/ads')
const DbHelper = require('../helpers/db')
const dbHelper = new DbHelper()
let config = ''
const cawer = new Cawer()
dotenv.config()
const cronJobsList = [];

(async () => {
	await dbHelper.connectToServer({
		MONGODB_SERVER: process.env.MONGODB_SERVER,
		MONGODB_PORT: process.env.MONGODB_PORT,
		MONGODB_DBNAME: process.env.MONGODB_DBNAME,
	})

	const dateSync = moment().tz('America/Los_Angeles')
		.format('YYYYMMDD')
	const dateReport = moment().tz('America/Los_Angeles')
		.subtract(10, 'days')
	config = await dbHelper.getConfig()
	await fs.ensureDirSync(path.join(ROOT_PATH, 'logs'))
	let reportData
	const adsReports = new AdsSpReports(config)
	// reportData = await adsReports.getCampaignReport(config.CREDENTIALS.ADVERTISING_PROFILE_ID, dateReport)
	// await dbHelper.insertDocument('adsSpCampaignsReports',
	// 	dateReport.format('YYYYMMDD'), reportData)
	//
	// reportData = await adsReports.getAdGroupReports(config.CREDENTIALS.ADVERTISING_PROFILE_ID, dateReport)
	// await dbHelper.insertDocument('adsSpAdGroupsReports',
	// 	dateReport.format('YYYYMMDD'), reportData)
	//
	// reportData = await adsReports.getKeywordsReports(config.CREDENTIALS.ADVERTISING_PROFILE_ID, dateReport)
	// await dbHelper.insertDocument('adsSpKeywordsReports',
	// 	dateReport.format('YYYYMMDD'), reportData)

	// reportData = await adsReports.getAsinsReports(config.CREDENTIALS.ADVERTISING_PROFILE_ID, dateReport)
	// await dbHelper.insertDocument('adsSpAsinsReports',
	// 	dateReport.format('YYYYMMDD'), reportData)

	// reportData = await adsReports.getProductAdsReports(config.CREDENTIALS.ADVERTISING_PROFILE_ID, dateReport)
	// await dbHelper.insertDocument('adsSpProductAdsReports',
	// 	dateReport.format('YYYYMMDD'), reportData)
	process.exit()
	// const adAsins = new AdsSpAsins(config)
	// const products = new Products(config)
	// const sellerAsins = await products.getAllSellerAsins(dbHelper)
	// const items = await adAsins.getSuggestedKeywordsForAllAsins(sellerAsins)
})()
