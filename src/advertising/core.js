// const path = require('path')
// const rootPath = path.join(__dirname, '..', '..')
const Logering = require('logering')
const rootPath = require('app-root-path').path
const path = require('path')
// eslint-disable-next-line import/no-dynamic-require
const AdvertisingApi = require(path.join(
	rootPath, 'lib', 'amz-ads-api',
	'src', 'index.js',
))
const Cawer = require('cawer')

class AdsCore {
	constructor(config, className) {
		this.logger = new Logering(className, config.LOGERING)
		this.adsApi = new AdvertisingApi(config.CREDENTIALS)
		this.config = config
		this.cawer = new Cawer()
	}
}

module.exports = AdsCore
