const path = require('path')
const Cawer = require('cawer')
const Logering = require('logering')
const AdsCore = require('./core')
const ADS_API = require('../../lib/amz-ads-api/src')

class AdsProfiles {
	constructor(config) {
		this.logger = new Logering('adsProfiles', config.LOGERING)
		this.adsApi = new ADS_API(config.CREDENTIALS)
		this.config = config
		this.cawer = new Cawer()
	}

	async getAll() {
		const profilesList = await this.adsApi.Profiles.listProfiles()

		// const databaseFile = `adsProfiles.json`
		// const databaseFolder = path.join(rootPath, 'database')
		// const databaseFullFilePath = path.join(databaseFolder, databaseFile)
		// jsonfile.writeFileSync(databaseFullFilePath, profilesList, { spaces: 2 })
		// console.log(`${options.action} data saved on database`)
		return profilesList
	}

}

module.exports = AdsProfiles
