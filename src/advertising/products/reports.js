const moment = require('moment-timezone')
const AdsCore = require('../core')
let reportDataList = []

class reports extends AdsCore {
	constructor(config) {
		super(config, 'reports')
	}

	mergeCampaignReports(rawReports) {
		const mergedReports = []
		const campaignsIdList = [ ...new Set(rawReports.map((item) => item.campaignId)) ]
		for (let i = 0; i < campaignsIdList.length; i += 1) {
			const campaignId = campaignsIdList[i]
			const reportsFilteredByCampaignId = rawReports
				.filter((item) => item
					.campaignId === [ ...new Set(rawReports.map((itemFiltered) => itemFiltered.campaignId)) ][i])
			const impressions = reportsFilteredByCampaignId.reduce((total, item) => total + item.impressions, 0)
			const clicks = reportsFilteredByCampaignId.reduce((total, item) => total + item.clicks, 0)
			const costs = reportsFilteredByCampaignId.reduce((total, item) => total + item.cost, 0)
			const conversions = reportsFilteredByCampaignId
				.reduce((total, item) => total + item.attributedConversions30d, 0)
			const conversionsSameSKU = reportsFilteredByCampaignId
				.reduce((total, item) => total + item.attributedConversions30dSameSKU, 0)
			const unitsOrdered = reportsFilteredByCampaignId
				.reduce((total, item) => total + item.attributedUnitsOrdered30d, 0)
			const sales = reportsFilteredByCampaignId.reduce((total, item) => total + item.attributedSales30d, 0)
			const salesSameSKU = reportsFilteredByCampaignId
				.reduce((total, item) => total + item.attributedSales30dSameSKU, 0)
			const unitsOrderedSameSKU = reportsFilteredByCampaignId
				.reduce((total, item) => total + item.attributedUnitsOrdered30dSameSKU, 0)
			mergedReports.push({
				campaignId,
				impressions,
				clicks,
				costs,
				conversions,
				conversionsSameSKU,
				unitsOrderedSameSKU,
				unitsOrdered,
				sales,
				salesSameSKU,
			})
		}

		return mergedReports
	}

	async getCampaignReport(profileId, date) {
		console.log(`Getting campaigns reports day ${date.format('YYYYMMDD')}`)
		const options = {
			segment: 'placement',
			reportDate: date.format('YYYYMMDD'),
			metrics: 'campaignName,campaignId,campaignStatus,campaignBudget,'
				+ 'impressions,clicks,cost,bidPlus,'
				+ 'attributedConversions30d,attributedConversions30dSameSKU,attributedUnitsOrdered30d,'
				+ 'attributedSales30d,attributedSales30dSameSKU',
		}

		reportDataList = [ ...await this.adsApi.SpReports.processReport(profileId, 'campaigns', options), ...reportDataList ]

		return reportDataList
	}

	async getAdGroupReports(profileId, date) {
		console.log(`Getting adGroup reports day ${date.format('YYYYMMDD')}`)
		const options = {
			reportDate: date.format('YYYYMMDD'),
			metrics: 'campaignName,campaignId,adGroupName,adGroupId,'
				+ 'impressions,clicks,cost,'
				+ 'attributedConversions30d,attributedConversions30dSameSKU,attributedUnitsOrdered30d,'
				+ 'attributedSales30d,attributedSales30dSameSKU',
		}

		reportDataList = [ ...await this.adsApi.SpReports.processReport(profileId, 'adGroups', options), ...reportDataList ]

		return reportDataList
	}

	async getKeywordsReports(profileId, date) {
		console.log(`Getting keywords reports day ${date.format('YYYYMMDD')}`)
		const options = {
			segment: 'query',
			reportDate: date.format('YYYYMMDD'),
			metrics: 'campaignName,campaignId,keywordId,keywordText,matchType,'
				+ 'impressions,clicks,cost,'
				+ 'attributedConversions30d,attributedConversions30dSameSKU,attributedUnitsOrdered30d,'
				+ 'attributedSales30d,attributedSales30dSameSKU',
		}

		reportDataList = [ ...await this.adsApi.SpReports.processReport(profileId, 'keywords', options), ...reportDataList ]

		return reportDataList
	}

	async getProductAdsReports(profileId, date) {
		console.log(`Getting product ads reports day ${date.format('YYYYMMDD')}`)
		const options = {
			segment: 'placement',
			reportDate: date.format('YYYYMMDD'),
			metrics: 'campaignName,campaignId,adGroupName,adGroupId,'
				+ 'impressions,clicks,cost,currency,asin,sku'
				+ 'attributedConversions30d,attributedConversions30dSameSKU,attributedUnitsOrdered30d,'
				+ 'attributedSales30d,attributedSales30dSameSKU',
		}

		reportDataList = [ ...await this.adsApi.SpReports.processReport(profileId, 'productAds', options), ...reportDataList ]

		return reportDataList
	}

	async getAsinsReports(profileId, date) {
		console.log(`Getting product ads reports day ${date.format('YYYYMMDD')}`)
		const options = {
			campaignType: 'sponsoredProducts',
			reportDate: date.format('YYYYMMDD'),
			metrics: 'campaignName,campaignId,adGroupName,adGroupId,keywordId,keywordText,'
				+ 'asin,otherAsin,sku,currency,matchType,'
				+ 'attributedUnitsOrdered30dOtherSKU,attributedSales30dOtherSKU',
		}

		reportDataList = [ ...await this.adsApi.SpReports.processReport(profileId, 'asins', options), ...reportDataList ]

		return reportDataList
	}

	async getCampaignReportLastWeek() {
		const endDateWeek = moment().subtract(1, 'weeks')
			.endOf('week')
		let loopDateWeek = moment().subtract(1, 'weeks')
			.startOf('week')
		const promises = []
		while (endDateWeek.diff(loopDateWeek, 'days') !== 0) {
			promises.push(this.getCampaignReport(this.config.CREDENTIALS.ADVERTISING_PROFILE_ID, loopDateWeek))
			loopDateWeek = loopDateWeek
				.add(1, 'days')
		}
		await Promise.all(promises)
			.then(() => {
				console.log('All reports done')
			})
			.catch((e) => {
				console.log(`Error: ${e}`)
			})

		return this.mergeCampaignReports(reportDataList)
	}

}

module.exports = reports
