const path = require('path')
const Cawer = require('cawer')
const Logering = require('logering')
const ADS_API = require('../../../lib/amz-ads-api/src')

class Campaigns {
	constructor(config) {
		this.logger = new Logering('adsCampaigns', config.LOGERING)
		this.adsApi = new ADS_API(config.CREDENTIALS)
		this.config = config
		this.cawer = new Cawer()
	}

	async getCampaignList() {
		return this.adsApi.SpCampaigns
			.getCampaigns(this.config.CREDENTIALS.ADVERTISING_PROFILE_ID, {})
	}

	async changeCampaignBid(profileId, campaignId, dailyBudget) {
		const options = [
			{
				campaignId,
				dailyBudget,
			},
		]

		return this.adsApi.SpCampaigns.updateCampaigns(profileId, options)
	}

	async getCampaignInfo(profileId, campaignId) {
		return this.adsApi.SpCampaigns.getCampaign(profileId, campaignId)
	}

	// async getAndSaveAll() {
	// 	const options = {
	// 		action: 'listCampaignsEx',
	// 		api: 'campaigns',
	// 	}
	// 	const profilesDbFilePath = path.join(rootPath, 'database', 'adsProfiles.json')
	// 	if (fs.existsSync(profilesDbFilePath)) {
	// 		const profilesList = jsonfile.readFileSync(profilesDbFilePath)
	// 			.filter((profile) => profile.accountInfo.type === 'seller')
	// 		for (let i = 0; i < profilesList.length; i += 1) {
	// 			const profile = profilesList[i]
	// 			const campaignsList = await this.adsApi.request({
	// 				api: 'campaigns',
	// 				action: 'listCampaignsEx',
	// 				profileId: profile.profileId,
	// 			})
	// 			const dbFile = `adsCampaigns-${profile.profileId}.json`
	// 			const dbFolder = path.join(rootPath, 'database')
	// 			const dbFullFilePath = path.join(dbFolder, dbFile)
	// 			jsonfile.writeFileSync(dbFullFilePath, campaignsList, { spaces: 2 })
	// 			console.log(`profile ${profile.profileId} campaigns [${options.action}] data saved on database`)
	// 		}
	// 	} else {
	// 		throw new Error('No profile database found, aborting')
	// 	}
	// }

}

module.exports = Campaigns
