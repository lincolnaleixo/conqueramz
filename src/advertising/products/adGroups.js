const jsonfile = require('jsonfile')
const path = require('path')
const fs = require('fs-extra')
// const rootPath = path.join(__dirname, '..', '..', '..')
const Cawer = require('cawer')
const Logering = require('logering')
const ADS_API = require('../../../lib/amz-ads-api/src')

class AdGroups {
	constructor(config) {
		this.logger = new Logering('adsGroups', config.LOGERING)
		this.adsApi = new ADS_API(config.CREDENTIALS)
		this.config = config
		this.cawer = new Cawer()
	}

	async getAll() {
		return this.adsApi.SpAdGroups.getAdGroups(this.config.CREDENTIALS.ADVERTISING_PROFILE_ID, {})
		// const options = {
		// 	action: 'listAdGroupsEx',
		// 	api: 'adGroups',
		// }
		// const profilesDbFilePath = path.join(rootPath, 'database', 'adsProfiles.json')
		// if (fs.existsSync(profilesDbFilePath)) {
		// 	const profilesList = jsonfile.readFileSync(profilesDbFilePath)
		// 		.filter((profile) => profile.accountInfo.type === 'seller')
		// 	for (let i = 0; i < profilesList.length; i += 1) {
		// 		const profile = profilesList[i]
		// 		const adGroupsList = await this.adsApi.request({
		// 			api: options.api,
		// 			action: options.action,
		// 			profileId: profile.profileId,
		// 		})
		// 		const dbFile = `adGroups-${profile.profileId}.json`
		// 		const dbFolder = path.join(rootPath, 'database')
		// 		const dbFullFilePath = path.join(dbFolder, dbFile)
		// 		jsonfile.writeFileSync(dbFullFilePath, adGroupsList, { spaces: 2 })
		// 		console.log(`profile ${profile.profileId} adGroups [${options.action}] data saved on database`)
		// 	}
		// } else {
		// 	throw new Error('No profile database found, aborting')
		// }
	}

	async getSuggestedKeywords() {
		return this.adsApi.SpAdGroups
			.getAdGroupSuggestedKeywords(this.config.CREDENTIALS.ADVERTISING_PROFILE_ID, {})
	}

}

module.exports = AdGroups
