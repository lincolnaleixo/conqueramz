const path = require('path')
const Cawer = require('cawer')
const Logering = require('logering')
const ADS_API = require('../../../lib/amz-ads-api/src')

class AdGroupKeywords {
	constructor(config) {
		this.logger = new Logering('adGroupsKeywords', config.LOGERING)
		this.adsApi = new ADS_API(config.CREDENTIALS)
		this.config = config
		this.cawer = new Cawer()
	}

	async getAll() {
		return this.adsApi.SpKeywords.getKeywords(this.config.CREDENTIALS.ADVERTISING_PROFILE_ID)
		// const options = {
		// 	action: 'listBiddableKeywordsEx',
		// 	api: 'keywords',
		// }
		// const profilesDbFilePath = path.join(rootPath, 'database', 'adsProfiles.json')
		// if (fs.existsSync(profilesDbFilePath)) {
		// 	const profilesList = jsonfile.readFileSync(profilesDbFilePath)
		// 		.filter((profile) => profile.accountInfo.type === 'seller')
		// 	for (let i = 0; i < profilesList.length; i += 1) {
		// 		const profile = profilesList[i]
		// 		const adGroupsList = await this.adsApi.request({
		// 			api: options.api,
		// 			action: options.action,
		// 			profileId: profile.profileId,
		// 		})
		// 		const dbFile = `${options.api}-${profile.profileId}.json`
		// 		const dbFolder = path.join(rootPath, 'database')
		// 		const dbFullFilePath = path.join(dbFolder, dbFile)
		// 		jsonfile.writeFileSync(dbFullFilePath, adGroupsList, { spaces: 2 })
		// 		console.log(`profile ${profile.profileId} ${options.api} [${options.action}] data saved on database`)
		// 	}
		// } else {
		// 	throw new Error('No profile database found, aborting')
		// }
	}

}

module.exports = AdGroupKeywords
