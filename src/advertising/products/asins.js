const path = require('path')
const fs = require('fs-extra')
// const rootPath = path.join(__dirname, '..', '..', '..')
const Cawer = require('cawer')
const Logering = require('logering')
const ADS_API = require('../../../lib/amz-ads-api/src')

class Asins {
	constructor(config) {
		this.logger = new Logering('adsAsin', config.LOGERING)
		this.adsApi = new ADS_API(config.CREDENTIALS)
		this.config = config
		this.cawer = new Cawer()
	}

	async getSuggestedKeywordsForAllAsins(asins) {
		const suggestedKeywords = []
		for (let i = 0; i < asins.length; i += 1) {
			const asinSuggestedKeywords = await this.adsApi.SpAsins
				.getSuggestedKeywordsByAsin(this.config.CREDENTIALS.ADVERTISING_PROFILE_ID, asins[i])
			suggestedKeywords.push({
				asin: asins[i], suggestedKeywords: asinSuggestedKeywords,
			})
		}

		return suggestedKeywords
	}

}

module.exports = Asins
