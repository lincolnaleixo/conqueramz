const AdsCampaigns = require('../advertising/products/campaigns')

class AdsAutomation {
	constructor(config) {
		this.config = config
		this.adsCampaigns = new AdsCampaigns(config)
	}

	async automateSpAdsCampaigns(campaignsReport) {
		let ACOS = 0
		let newDailyBudget = 1
		const automationConfig = this.config.AUTOMATION.ADS.CAMPAIGNS
		for (let i = 0; i < campaignsReport.length; i += 1) {
			const campaignReport = campaignsReport[i]
			const actualCampaignInfo = await this.adsCampaigns
				.getCampaignInfo(this.config.ADS.PROFILE_ID, campaignReport.campaignId)
			if (campaignReport.costs > 0 && campaignReport.sales === 0) ACOS = -1
			else ACOS = (parseFloat(campaignReport.costs) / parseFloat(campaignReport.sales)) * 100
			if (ACOS === automationConfig.ITEM_VALUE_EQUAL) {
				newDailyBudget = actualCampaignInfo.dailyBudget * automationConfig.CHANGE_VALUE_EQUAL
			} else if (ACOS <= automationConfig.ITEM_VALUE_LESS_THAN) {
				newDailyBudget = actualCampaignInfo.dailyBudget * automationConfig.CHANGE_VALUE_LESS_THAN
			} else if (ACOS > automationConfig.ITEM_VALUE_GREATER) {
				newDailyBudget = actualCampaignInfo.dailyBudget * automationConfig.CHANGE_VALUE_GREATER
			}
			await this.adsCampaigns
				.changeCampaignBid(this.config.ADS.PROFILE_ID, campaignReport.campaignId, newDailyBudget)
			console.log(`Changed ${actualCampaignInfo.name} dailyBudget from ${actualCampaignInfo.dailyBudget} to ${newDailyBudget}`)
		}
	}
}

module.exports = AdsAutomation
