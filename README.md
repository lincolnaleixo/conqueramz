## ConquerAMZ

### Why? There's a lot of good software available online

Mainly because I don't trust my data to other sellers (they're our competitors anyway). I wanted to make a solution that
runs in your server, without anyone being able to access your precious data.

This is a try to be an alternative to the paid and hosted solutions already available. Developing software is very
difficult and takes a lot of time, so it may take some time until we reach the level where we can ditch any other
solution

Any help will be very much appreciated!