

FROM node:lts-stretch-slim as builder
ARG project=conqueramz
WORKDIR $project
COPY ./package.json .
COPY ./yarn.lock .
RUN yarn --production && yarn cache clean --force
RUN npm install pm2 -g

FROM node:lts-stretch-slim
ARG project=conqueramz
COPY --from=builder . .

ENV TZ=America/Los_Angeles

COPY ./lib $project/lib
COPY ./src $project/src
COPY ./resources $project/resources
COPY ./views $project/views
COPY ./public $project/public
COPY ./apps.json $project
COPY app.js $project
COPY jobs.js $project

WORKDIR $project

ENTRYPOINT ["pm2-runtime", "start", "apps.json"]
